# GESTION FDES pour BE

[ETAMINE](https://www.etamine.coop) et [AMOES](https://www.amoes.com) sont deux bureaux d'études et de conseils engagés pour la performance environnementale du bâti et de ses abords. Pratiquant la coopération au quotidien dans nos deux SCOP, nous avons mis en commun nos moyens pour faire développer un script informatique.

Ce script,« INIES to XLS », facilite l’exploitation sous Excel des données de la base nationale INIES, qui regroupe les Fiches de Déclarations environnementale et Sanitaires (FDES) des produits de constructions. Ces données sont notamment utilisées dans le cadre de la vérification de la conformité des constructions à la RE2020.

Convaincus que l’avenir est au partage, nous proposons aujourd’hui ce script, développé avec [@Paul Etienney](https://www.linkedin.com/in/pauletienney/), au plus grand nombre, via un dépôt sur « GitLab », sous licence libre LGPL et CeCILL-B.

Pour l’utiliser, un abonnement au webservice INIES en cours de validité est nécessaire.

Les fonctionnalités proposées par ce script sont notamment :  
* Extraction automatique de la base de données INIES vers Excel (une ligne par FDES et une colonne par donnée extraite). Ces extractions peuvent être automatisées en utilisant les planifications de tâches de votre système d'exploitation.
* Conservation des données extraites depuis la première utilisation de l’outil (fonction d’historicisation).
* Téléchargement en une fois d’une sélection de FDES

Il facilite ainsi la consultation d’INIES, sans solution logicielle payante, ce qui peut à notre sens intéresser l’ensemble des acteurs de la construction (Programmistes, Architectes, Bureaux d’études, Entreprises, etc…) pour participer à notre objectif commun : réduire l’impact carbone du bâti et de ses abords.

>A noter que nous ne serons pas en mesure de gérer l’administration en exploitation de ce projet libre. Aussi, nous vous invitons à copier/coller/utiliser/forker ce code pour vos besoins et si vous le souhaitez nous faire vos remarques et retours pour aider la communauté, sans engagement de coordination de notre part.

Bonne utilisation ! 

## Licences
Le code contenu dans ce dépôt est distribué sous licences LGPL et son équivalent CeCILL-B

* lire les fichiers COPYING.txt et COPYING.LESSER.txt pour la licence LGPL
* lire les fichiers Licence_CeCILL-B_V1-fr.txt pour la licence CeCILL-B
  

## Gestion FDES

### Installation

````commandline
git clone git@gitlab.com:etamine-coop/outils-productions/etamine-fdes.git fdes 
cd fdes
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
````

#### Configuration du .env

Il faut mettre en place l'environnement local. 
Lancez le fichier ```setup.py```.
Renseigner les paramètres demandés dans votre terminal, le fichier se crée automatiquement.

### Utilisation
Avant de faire tourner les scripts, il faut activer le virtual environnement. Placez-vous à la racine du projet et faites : 

````commandline
source venv/bin/activate
````

#### Import initial
Depuis le répertoire ```code``` situé à la racine du projet, faites :
```commandline
python import_full.py
```
 
#### Import incrémental

Depuis le répertoire ```code``` situé à la racine du projet, faites :
```commandline
python import_update.py --days=10
```
Le script import_update.py récupère les FDES mis à jours lors des X derniers jours (arguments --days). Ce nombre est configurable depuis le code du script.

#### Extraction
Depuis le répertoire ```code``` situé à la racine du projet, faites :
````commandline
python extract.py
````
Il s'agit d'un script d'exemple permettant de filtrer les FDES.

#### Téléchargement des PDF FDES
Mettre en place un document excel qui comprend en colonnes les données suivantes
- Nom du composant de la forme [Batiment]_[lot]_[Nom du composant]
- ID de la FDES
- Nom des fichiers téléchargés (colonne laissée vide et remplie par le script)

Dans le .env, renseigner les variable d'environnement :
- PATH_METRE avec le chemin du fichier Excel
- PATH_ARCHIVE avec le chemin du dossier où les archives seront créées
- COL_COMPOSANTS avec l'entête de la colonne composant 
- COL_ID avec l'entête de la colonne ID
- COL_FILE avec l'entête de la colonne fichier

Depuis le répertoire ```code``` situé à la racine du projet, faites :
````commandline
python download_pdf.py
````
Le script download_pdf.py télécharge les PDF des fiches renseignées dans le métré et les range dans des dossiers par lots puis zippe ces dossiers. 
Le résultat consiste en autant d'archive que de lots renseignées dans le métré et un fichier general.xlsx récapitulatif des noms de fichiers récupérés et des éventuelles erreurs de téléchargement. 

Attention, les fiches par défaut ne sont pas téléchargeables via ce script.


### Données

Les fichiers de référence (pivot) sont les fichiers JSON présents dans le répertoire data/raw_pivot. Ils contiennent les FDES historisés. Ce sont ces fichiers qui sont utilisés pour le processus de mise à jour et d'historisation des FDES.

Je vous encourage à backuper régulièrement ces fichiers car ils constituent une simili-base de données. Si ils devaient être perdus, c'est tout l'historique des FDES qui serait également perdu.

Le document Excel est un sous-produit de ces données pivot.

### Code

#### Organisation du code

Le code est organisés en étapes (steps) successives. Chaque étape a vocation a recevoir des données d'entrée de la step précédente, de les traiter et de les mettre à disposition de la suivante.

#### Director

C'est la class qui orchestre les steps pour obtenir le résultat souhaité.

#### Steps disponibles

- Inies : récupération des données depuis le webservice INIES (fdes, normes, nomenclatures)
- RawUpdateLocal : écrit et lit les données brutes de mises à jour incrémentale dans un fichier JSON local
- **RawPivotLocal** : écrit et lit les données brutes pivot dans un fichier JSON local. Ces fichiers sont les références pour les mises à jour.
- IniesFlattener : transforme les données brutes en données mises à plat et préparées pour le fichier Excel
- ExcelLocal : écrit et lit les données sur le fichier Excel 
- Filter : filtre les données FDES pour ne garder que celles correspondant au filtre demandé
- Download : permet de télécharger les PDF de FDES à partir d'un excel

### Tests

Un premier test dédié au IniesMerger a été mis en place. Pour exécuter les tests, placez-vous dans le répertoire ````code```` du projet puis :

```commandline
python -m unittest discover tests
```

### Logs

Un fichier de log est disponible au chemin ```logs/fdes.log```. Le niveau de verbosité des logs est défini dans le fichier ````.env````. Les logs contiennent les exceptions pouvant survenir lors de l'exécution d'un des scripts : 

- import_full.py
- import_update.py
- extract.py
- download_pdf.py
