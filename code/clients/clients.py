# -*- coding: utf-8 -*-
# Copyright (C) 2024 ETAMINE
# 
# gestion-fdes is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gestion-fdes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with etamine-fdes. If not, see <http://www.gnu.org/licenses/>.

import zeep.exceptions
from zeep import helpers
from zeep import Client
from dotenv import load_dotenv
import os
from datetime import datetime


class Inies:

    def __init__(self):

        self.__login = None
        self.__password = None
        self.__auth_token = None
        self.__client = None

        self.init_client()

    def init_client(self):

        load_dotenv()

        self.__client = Client(os.getenv('FDES_URL'))
        self.__login = os.getenv('FDES_LOGIN')
        self.__password = os.getenv('FDES_PASSWORD')

    def __authenticate(self, force=False):

        if self.__auth_token is None or force:
            try:
                self.__auth_token = self.__client.service.Login(
                    self.__login, self.__password)
            except zeep.exceptions.Fault:
                # TODO : handle wrong login and do not silence the error
                # we have to close the session before
                # demanding a new auth token

                self.__close_session()
                self.__authenticate(force=True)

    def __close_session(self):

        self.__client.service.CloseSessionByUserLogin(self.__login)

    def get_change_list(self, since: datetime):

        self.__authenticate()
        return self.__client.service.GetLastUpdatedFDESShortData(
            self.__auth_token, since)

    def get_fdes_full_data(self, fdes_id):

        self.__authenticate()
        full = self.__client.service.GetFDESFullDataByID(fdes_id,
                                                         self.__auth_token)

        return helpers.serialize_object(full, dict)

    def get_nomenclature(self):

        self.__authenticate()
        r = self.__client.service.GetNomenclature(self.__auth_token)

        return helpers.serialize_object(r, dict)

    def get_norme(self, id):

        self.__authenticate()
        norme = self.__client.service.GetNormeByID(id, self.__auth_token)

        return helpers.serialize_object(norme, dict)

    def get_all_short_data(self):

        self.__authenticate()
        r = self.__client.service.GetAllFDESShortData(self.__auth_token)

        return helpers.serialize_object(r, dict)

    def get_fdes_file_name(self, fdes_id):

        self.__authenticate()
        return self.__client.service.GetFDESFileName(self.__auth_token,
                                                     fdes_id)

    def get_file_content(self, fdes_file_name):

        self.__authenticate()
        return self.__client.service.GetFileContent(self.__auth_token,
                                                    fdes_file_name)
