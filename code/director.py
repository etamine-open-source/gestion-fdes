# -*- coding: utf-8 -*-
# Copyright (C) 2024 ETAMINE
# 
# gestion-fdes is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gestion-fdes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with etamine-fdes. If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
from helpers import make_dirs
from steps.inies_flattener import IniesFlattener
from steps.inies_merger import IniesMerger
from steps.steps import (RawUpdateLocal, RawPivotLocal,
                         Inies, Step, ExcelLocal, Filter,
                         Download)
from dotenv import load_dotenv
import os


class Director:

    def __init__(self):
        make_dirs()

    def full_import(self):

        # Step 1 : load for the web service
        inies = Inies()
        inies.read()

        # Step 2 : merge new data (all fdes for first import)
        # and old data (empty for first import)
        merger = IniesMerger()
        merger.transfer_new_data(inies)
        merger.merge()

        # Step 3 : save this full import to the pivot files
        raw_pivot = RawPivotLocal()
        self.transfer(merger, raw_pivot)
        raw_pivot.save()

        # Step 5 : flatten pivot data
        flattener = IniesFlattener()
        self.transfer(raw_pivot, flattener)
        flattener.flatten()
        #
        # # Step 4 : save data to Excel file
        excel_local = ExcelLocal()
        self.transfer(flattener, excel_local)
        excel_local.build_file()

    def import_since(self, since: datetime):

        # Step 1 : we fetch the new data since a date
        inies = Inies()
        inies.read_since(since)

        # Step 1.1 (optional) : we save this new data to the raw update file
        raw_update = RawUpdateLocal()
        self.transfer(inies, raw_update)
        raw_update.save()

        # Step 2 : we get the pivot data (the result of the last import)
        raw_pivot = RawPivotLocal()
        raw_pivot.read()

        # Step 3 : we get the whole list of FDES ids
        # so we can check if we need to deprecate some
        all_fdes_ids = inies.get_all_fdes_ids()

        # Step 4 : we inject pivot data, new data and list of published ids
        # into the merger
        merger = IniesMerger()
        self.transfer(raw_pivot, merger)
        merger.transfer_new_data(raw_update)
        merger.published_fdes_ids = all_fdes_ids
        merger.merge()

        # Step 5 : we save the newly merged data into a file
        # (it will be used as our reference for the next import)
        self.transfer(merger, raw_pivot)
        raw_pivot.save()

        # Step 6 : we flatten and prepare the pivot data to be saved to excel
        flattener = IniesFlattener()
        self.transfer(raw_pivot, flattener)
        flattener.flatten()

        # Step 7 : we save the flattened data to excel
        excel_local = ExcelLocal()
        self.transfer(flattener, excel_local)
        excel_local.build_file()

    # Method : filter_fdes
    # filters : dict of multiple field_name -> field_value to look in each FDES
    # eg : {'Etat': 'Valide'} all FDES with Etat fields equal to 'Valide'
    # NB : you can use key>>sub_key>>sub_sub_key syntax to target nested fields
    # eg : {'Indicateur_Set>>Norme_ID': 2}
    def filter_fdes(self, filters: dict) -> list:

        # Step 1 : we get the most recent pivot data
        raw_pivot = RawPivotLocal()
        raw_pivot.read()

        # Step 2 : we keep only the fdes that match the filters
        fdes_filter = Filter()
        self.transfer(raw_pivot, fdes_filter)
        fdes_filter.filter_fdes(filters)

        # Step 3 : we return this filtered list of fdes
        return fdes_filter.fdes

    def create_archives(self):

        load_dotenv()

        download = Download()
        download.set_fdes_dict(os.getenv('PATH_METRE'))
        download.set_archive_path(os.getenv('PATH_ARCHIVE'))

        inies = Inies()

        col_composants = os.getenv('COL_COMPOSANTS')
        col_id = os.getenv('COL_ID')
        col_file = os.getenv('COL_FILE')

        for index, row in download.FDES.iterrows():
            lot = "_".join([str(item) for item in row[col_composants
                                                      ].split('_')[0:2]])
            id = str(row[col_id]).split('_')[0]
            path_archive_lot = download.create_archive_dir(lot)
            if download.fdes_in_cache(id):
                end_pdf_name = download.copy_pdf(id, path_archive_lot)
            else:
                file_name = inies.get_pdf_name(id)
                if file_name is None:
                    end_pdf_name = "file name not found"
                    download.add_error(id, end_pdf_name)
                else:
                    file_content = inies.get_pdf_content(file_name)
                    if file_content is None:
                        end_pdf_name = "file content not found"
                        download.add_error(id, end_pdf_name)
                    else:
                        end_pdf_name = download.set_pdf_name(id, file_name)
                        path_pdf = inies.save_pdf(file_content, end_pdf_name,
                                                  path_archive_lot)
                        download.add_fdes_in_cache(id, path_pdf)
            download.add_line_in_lot_pdf(lot, row, end_pdf_name)
            download.FDES.at[index, col_file] = end_pdf_name

        download.zip_all_dirs()
        download.print_error_dict()
        download.print_fdes_dict()

    # playground to test/dev things with the Director
    def sandbox(self):

        # Try new things

        pass

    @staticmethod
    def transfer(source: Step, target: Step):

        target.set_fdes(source.get_fdes())
        target.set_normes(source.get_normes())
        target.set_nomenclature(source.get_nomenclature())
        target.set_indics(source.get_indics())
        target.set_phases(source.get_phases())
