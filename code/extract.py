# -*- coding: utf-8 -*-
# Copyright (C) 2024 ETAMINE
# 
# gestion-fdes is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gestion-fdes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with etamine-fdes. If not, see <http://www.gnu.org/licenses/>.

from pprint import pprint
from director import Director
from helpers import get_logger


def run():

    logger = get_logger()

    try:

        d = Director()
        data = d.filter_fdes({
            'Indicateur_Set>>Norme_ID': 2
        })

        # do whatever you want with data
        pprint(f"Found {len(data)} FDES")

    except Exception as e:

        logger.exception(e)
        raise e


if __name__ == '__main__':
    run()
