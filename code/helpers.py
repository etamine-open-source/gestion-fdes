# -*- coding: utf-8 -*-
# Copyright (C) 2024 ETAMINE
# 
# gestion-fdes is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gestion-fdes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with etamine-fdes. If not, see <http://www.gnu.org/licenses/>.

import datetime
import json
import logging
from logging.handlers import RotatingFileHandler
import os
import sys
from functools import lru_cache
from os import path

from dotenv import load_dotenv


def nip_code(norme_id: int, indicateur_id: int, phase_id: int):
    return f'{norme_id}_{indicateur_id}_{phase_id}'


def clean_sheet_name(name: str):

    to_remove = ['/', '\\', '?', '*', '[', ']']

    for c in to_remove:
        name = name.replace(c, '_')

    return name


# All path must be relative to the root of the project (code folder)
def abs_path(rel_path: str):

    base = path.dirname(path.abspath(str(sys.modules['__main__'].__file__)))
    return path.join(base, rel_path)


def make_dirs():

    load_dotenv()

    env_path_vars = [
        'PATH_EXCEL_LOCAL',
        'PATH_RAW_UPDATE',
        'PATH_RAW_PIVOT',
        'PATH_LOGS'
    ]

    for path_var in env_path_vars:

        path = os.getenv(path_var)
        os.makedirs(abs_path(path), exist_ok=True)


def get_logger():

    make_dirs()

    load_dotenv()
    # TODO recherche sur os.getenv : configurable ?
    log_lvl = os.getenv('LOG_LEVEL')
    log_folder = abs_path(os.getenv('PATH_LOGS'))
    log_path = f'{log_folder}/fdes.log'

    log_formatter = logging.Formatter('%(levelname)s' +
                                      ' @ %(asctime)s : %(message)s',
                                      '%d/%m/%Y %H:%M:%S')
    logHandler = RotatingFileHandler(log_path, mode='a', maxBytes=256*1024,
                                     backupCount=2, encoding=None, delay=0)

    logHandler.setFormatter(log_formatter)
    logHandler.setLevel(log_lvl)

    logger = logging.getLogger('fdes')
    logger.setLevel(logging.INFO)

    logger.addHandler(logHandler)

    return logger


@lru_cache
def get_mapping():

    with open(abs_path('./../mapping.json')) as f:
        mapping = json.load(f)

    return mapping


def dt_to_str(dt: datetime):

    load_dotenv()
    format = os.getenv('DT_FORMAT')

    return dt.strftime(format)


def now_to_str():

    return dt_to_str(datetime.datetime.now())


@lru_cache
def str_to_dt(dt_str: str):

    load_dotenv()
    format = os.getenv('DT_FORMAT')

    return datetime.datetime.strptime(dt_str, format)


class IniesJSONEncoder(json.JSONEncoder):

    def default(self, o):

        if isinstance(o, datetime.datetime):
            return dt_to_str(o)

        return super().default(o)


class IniesJSONDecoder(json.JSONDecoder):

    # list all datetime fields, so they are converted on read and save
    DT_FIELDS = [
        'import date',  # fields created by the merger
        'Date obsolescence',  # fields created by the merger
        'Last_Update_Date'
    ]

    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(self,
                                  object_hook=self.dict_to_object,
                                  *args,
                                  **kwargs)

    def dict_to_object(self, d):

        for field in self.DT_FIELDS:
            if field in d and d[field] is not None:
                d[field] = str_to_dt(d[field])

        return d
