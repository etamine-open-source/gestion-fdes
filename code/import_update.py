# -*- coding: utf-8 -*-
# Copyright (C) 2024 ETAMINE
# 
# gestion-fdes is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gestion-fdes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with etamine-fdes. If not, see <http://www.gnu.org/licenses/>.

import argparse
from mailer import Mail
from datetime import datetime, timedelta
from helpers import get_logger
from director import Director

from dotenv import load_dotenv
import os


def run():

    logger = get_logger()

    try:
        logger.info("Lancement import_update")
        # Get command --days argument
        parser = argparse.ArgumentParser()
        parser.add_argument('--days', required=False, default=7, type=int)
        args = parser.parse_args()

        # Get the date of the period we want to import
        since = datetime.now() - timedelta(days=args.days)

        d = Director()
        d.import_since(since)
        logger.info("Fin import_update")

    except Exception as e:
        logger.exception(e)

        load_dotenv()
        mailer_password = os.getenv('MAILER_PASSWORD')
        if mailer_password and mailer_password != "":
            email = Mail()
            configurate_email(email)
            email.message = f"Une erreur est survenue lors de la mise a \
                jour de la base FDES :\n{e}\n\nVoir les logs sur le \
                poste de developpement"
            email.object = "[FDES] Erreur lors de la mise à jour de \
                la base"
            email.send()

        raise e


def configurate_email(email):
    '''
    Récupère la configuration du mailer dans les variables d'environnement

    Paramètres :
        email : Mail    objet Mail à envoyer
    Retour :
        Aucun, agit directement sur l'objet email
    '''
    load_dotenv()

    email.smtp = os.getenv('MAILER_SMTP')
    email.port = int(os.getenv('MAILER_PORT'))
    email.user = os.getenv('MAILER_USER')
    email.password = os.getenv('MAILER_PASSWORD')
    email.recipient = os.getenv('MAILER_RECIPIENT')


if __name__ == '__main__':
    run()
