# -*- coding: utf-8 -*-
# Copyright (C) 2024 ETAMINE
# 
# gestion-fdes is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gestion-fdes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with etamine-fdes. If not, see <http://www.gnu.org/licenses/>.

import smtplib
from email.mime.text import MIMEText

class Mail:
    """
    Classe d'envoi de mail
    """

    def __init__(self):
        self.object = ""
        self.message = ""
        self.smtp = ""
        self.port = 0
        self.user = ""
        self.password = ""
        self.recipient = ""

    def send(self):
        '''
        Envoie le mail défini dans l'objet self
        '''
        msg = MIMEText(self.message)
        msg['Subject'] = self.object
        msg['From'] = self.user
        msg['To'] = self.recipient

        # Envoi du message

        with smtplib.SMTP(self.smtp, self.port) as server:
            # encodage STARTTLS
            server.starttls()
            # connexion
            server.login(self.user, self.password)
            # envoi du mail
            server.sendmail(self.user, self.user, msg.as_string())
