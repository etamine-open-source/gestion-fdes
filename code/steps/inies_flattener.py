# -*- coding: utf-8 -*-
# Copyright (C) 2024 ETAMINE
# 
# gestion-fdes is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gestion-fdes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with etamine-fdes. If not, see <http://www.gnu.org/licenses/>.

from functools import lru_cache

from helpers import nip_code, get_mapping, dt_to_str
from progress.bar import Bar
from steps.steps import Step


# ####################
# ####################
# This class converts raw data from INIES into a flat (dict) format
# ####################
# ####################

class IniesFlattener(Step):

    FIELDS_TO_SKIP = [
        'Online_Date',
        'isPEP',
        'Issue_Date',
        'isVerified',
        'Commercial_References_Number',
        'UF_Unit_ID',
        'Implementation_Fall_Rate',
        'Maintenance_Frequency',
        'Health_Data_Is_Contact_Drinking_Water',
        'Health_Data_Is_Contact_Not_Drinking_Water',
        'Health_Data_Health_Number',
        'Comfort_Data',
        'Responsible_Organism_Address',
        'Classification_ID_2',
        'Classification_ID_3',
        'Production_Region_Fr'
    ]

    CAT_LVL_0 = 'Catégorie niv. 0'
    CAT_LVL_1 = 'Catégorie niv. 1'
    CAT_LVL_2 = 'Catégorie niv. 2'
    CAT_LVL_3 = 'Catégorie niv. 3'
    CAT_LVL_4 = 'Catégorie niv. 4'

    def __init__(self):

        super().__init__()

        # Flat data
        self.fdes = []
        self.normes = []
        self.nomenclature = []

        # Prep work / memoization fields
        self.nips_labels = {}
        self.normes_names = {}

    def flatten(self):

        # We have to restructure some info for easier use
        self.__prep_work()

        # We have done all the prep work, now we can flatten the data
        self.flatten_all_fdes()

        self.flatten_indics()
        self.flatten_phases()

        self.flatten_normes()

    def flatten_indics(self):

        res = []

        for n in self.normes:
            n_name = n['Norme_Name']
            n_id = n['Norme_ID']

            for indic in n['List_Indicateur_Descriptions'][
                    'Indicateur_Description']:

                item = {
                    'Norme_ID': n_id,
                    'Norme_Name': n_name
                }
                item.update(indic)

                res.append(item)

        self.indics = res

    def flatten_phases(self):

        res = []

        for n in self.normes:
            n_id = str(n['Norme_ID'])
            n_name = n['Norme_Name']

            for phase in n['List_Phase_Descriptions']['Phase_Description']:

                item = {
                    'Norme_ID': n_id,
                    'Norme_Name': n_name
                }
                item.update(phase)

                res.append(item)

        self.phases = res

    def __prep_work(self):

        # #########
        # Prep work
        # #########

        # Convert Norme/Indicateur/Phase (nip) into a nip_identifier:
        # nip_label dict
        self.__prep_nips_labels()

        # Get the list of normes names in a dict
        self.__prep_normes_names()
        pass

    def flatten_normes(self):

        res = []

        for n in self.normes:
            res.append(self.__flatten_one_norme(n))

        self.normes = res

    @staticmethod
    def __flatten_one_norme(n):

        return {
            'id': n['Norme_ID'],
            'name': n['Norme_Name']
        }

    def __prep_normes_names(self):

        for n in self.normes:
            n_id = str(n['Norme_ID'])
            self.normes_names[n_id] = n['Norme_Name']

    def __prep_nips_labels(self):

        # Prep work
        # Building the catalogue of all normes/indicateurs/phases combos (nips)
        for n in self.normes:

            id = n['Norme_ID']

            id_str = str(id)
            if id_str not in self.nips_labels:
                self.nips_labels[id_str] = {}

            indics = [i for i in n['List_Indicateur_Descriptions'][
                'Indicateur_Description']]
            phases = [p for p in n['List_Phase_Descriptions'][
                'Phase_Description']]

            for ind in indics:
                for phas in phases:
                    code = nip_code(id, ind['Indicateur_ID'], phas['Phase_ID'])

                    if code not in self.nips_labels:
                        self.nips_labels[
                            id_str][code] = f"{ind['Indicateur_Name']} " \
                                f"- {phas['Phase_Name']} " \
                                f"({ind['Indicateur_Unit']}) - (({code}))"

            del indics
            del phases

    def flatten_all_fdes(self):

        res = []

        fdes = list(filter(lambda x: x is not None, self.fdes))

        with Bar('Flattening FDES', max=len(fdes)) as bar:

            for raw in fdes:
                flat_row = self.__flatten_one_fdes(raw)
                res.append(flat_row)

                bar.next()

        bar.finish()

        self.fdes = res

    def __flatten_one_fdes(self, raw):

        flat = {}

        for key, val in raw.items():
            flat_prop = self.__flatten_prop(key, val)
            if flat_prop is not None:
                for k, v in flat_prop:
                    flat[k] = v

        return flat

    # Each field of each FDES goes through this function
    # Based on the field name (key) and the value type
    # we choose a specific method to flatten the value
    # Each of these methods returns a list of tuples (key, value)
    def __flatten_prop(self, key, val):

        # We skip some fields
        if key in self.FIELDS_TO_SKIP:
            return None

        # If value is empty, we flatten to empty string
        if val is None:
            return self.flatten_none(key)

        func_name_base = "flatten_"

        # check if we have a dedicated method for this specific field
        func_name = f"{func_name_base}{key.lower()}"

        if self.has_method(func_name):
            return getattr(self, func_name)(key, val)

        # else we use the type of the value to determine the treatment
        func_name = func_name_base + type(val).__name__
        if self.has_method(func_name):
            return getattr(self, func_name)(key, val)

        # Finally, we found nothing to handle this field. We raise an error
        raise ValueError(f'key {key} is not handled (type : {type(val)})')

    @lru_cache
    def flatten_declaration_type(self, key, val):

        mapping = get_mapping()
        decl_type = next((d for d in mapping['Declaration_type']
                          if d['id'] == val), None)

        if decl_type is None:
            raise ValueError(
                f'No declaration type found for id {val} in mapping')

        return [(key, decl_type['label'])]

    # Handle the nomenclature field
    @lru_cache
    def flatten_classification_id(self, key, val):

        nomenc_list = self.build_nomenc(val)

        lvls = {
            self.CAT_LVL_0: '',
            self.CAT_LVL_1: '',
            self.CAT_LVL_2: '',
            self.CAT_LVL_3: '',
            self.CAT_LVL_4: ''
        }

        for nomenc in nomenc_list:

            if nomenc is None:
                continue

            if nomenc['Tree_Level'] == 3:
                lvls[self.CAT_LVL_4] = nomenc['Nomenclature_Item_Name']

            if nomenc['Tree_Level'] == 2:
                lvls[self.CAT_LVL_3] = nomenc['Nomenclature_Item_Name']

            if nomenc['Tree_Level'] == 1:
                lvls[self.CAT_LVL_2] = nomenc['Nomenclature_Item_Name']

            if nomenc['Tree_Level'] == 0 and isinstance(
                    nomenc['Parent_Item_ID'], int):
                lvls[self.CAT_LVL_1] = nomenc['Nomenclature_Item_Name']

            if nomenc['Tree_Level'] == 0 and nomenc['Parent_Item_ID'] is None:
                lvls[self.CAT_LVL_0] = nomenc['Nomenclature_Item_Name']

        return [(label, val) for label, val in lvls.items()]

    # Handle the constituants field
    @staticmethod
    def flatten_list_fu_constituant_products(key, val):

        c_list = []
        if val.get('FU_Constituant_Product', None) is not None:
            c_list = [f"{c['Name']} : {c['Quantity']} {c['Unit']}"
                      for c in val['FU_Constituant_Product']]

        return [(key, ' | '.join(c_list))]

    # Handle the indicateur_set field
    # This is where the majority of columns are built
    def flatten_indicateur_set(self, _, val):

        res = []

        # Fist handle the norme infos (which is a part of the indicateur_set)
        norme_id = str(val['Norme_ID'])
        res += self.flatten_normes_infos(norme_id)

        # Then we work on this norme indicators
        norme_nips_labels = self.nips_labels[norme_id]
        fdes_nips_qty = self.get_one_fdes_nips_qty(val)

        for code, label in norme_nips_labels.items():

            if code in fdes_nips_qty:
                res.append((f"{label}", fdes_nips_qty[code]))
            else:
                res.append((f"{label}", ''))

        return res

    @staticmethod
    def get_one_fdes_nips_qty(indics: dict):

        nips = {}

        n_id = indics['Norme_ID']

        # Test indicateurs <> None
        if indics['List_Indicateur_Quantities']:

            for i in indics['List_Indicateur_Quantities'][
                    'Indicateur_Quantity']:
                code = nip_code(n_id, i['Indicateur_ID'], i['Phase_ID'])
                qty = i['Quantity']

                nips[code] = qty

        return nips

    def flatten_normes_infos(self, norme_id: str):

        if not isinstance(norme_id, str):
            raise ValueError(
                f'norme_id must be a string, not {type(norme_id)}')

        return [
            ('Norme_ID', norme_id),
            ('Norme_Name', self.normes_names[norme_id])
        ]

    @staticmethod
    def flatten_str(key, val):
        return [(key, val)]

    @staticmethod
    def flatten_int(key, val):
        return [(key, val)]

    @staticmethod
    def flatten_float(key, val):
        return [(key, val)]

    @staticmethod
    def flatten_datetime(key, val):
        val_str = dt_to_str(val)
        return [(key, val_str)]

    @staticmethod
    def flatten_bool(key, val):
        bool_str = "Oui" if val else "Non"
        return [(key, bool_str)]

    @staticmethod
    def flatten_none(key):
        return [(key, '')]

    @staticmethod
    def flatten_dict(key, val):
        couples = []
        for k, v in val.items():
            merged_key = key + '_' + k
            couples.append((merged_key, v))
        return couples

    def has_method(self, func_name):
        return hasattr(self, func_name) and callable(getattr(self, func_name))

    def build_nomenc(self, id: int) -> list:

        res = []

        nomenc = self.get_nomenc_by_id(id)

        if nomenc is not None:

            res.append(nomenc)

            if isinstance(nomenc['Parent_Item_ID'], int):
                res.extend(self.build_nomenc(nomenc['Parent_Item_ID']))

        return res

    def get_nomenc_by_id(self, id: int) -> dict:

        for n in self.nomenclature:
            if n['Nomenclature_Item_ID'] == id:
                return n

        return None
