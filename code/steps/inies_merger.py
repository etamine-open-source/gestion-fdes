# -*- coding: utf-8 -*-
# Copyright (C) 2024 ETAMINE
# 
# gestion-fdes is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gestion-fdes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with etamine-fdes. If not, see <http://www.gnu.org/licenses/>.

from steps.steps import Step

from helpers import now_to_str


class IniesMerger(Step):

    STATE_VALID = 'Valide'
    STATE_DEPRECATED = 'Obsolète'

    COL_SOURCE = 'Source'
    COL_IMPORT_DATE = 'import date'
    COL_STATE = 'Etat'
    COL_DEPRECATED_DATE = 'Date obsolescence'

    def __init__(self):
        super().__init__()

        self.new_fdes = []
        self.new_normes = []
        self.new_nomenclature = []

        self.published_fdes_ids = []

    def merge(self):

        self.merge_fdes()
        self.deprecate_unused_fdes()

        self.merge_normes()
        self.merge_nomenclature()

        pass

    def merge_fdes(self):

        for new in self.new_fdes:
            if new:

                double = False

                new = self.add_extra_fields(new)

                for piv_idx, piv in enumerate(self.fdes.copy()):

                    if (new['Serial_Identifier'] == piv['Serial_Identifier'] and
                            new['Version'] != piv['Version']):

                        if new['Version'] > piv['Version']:

                            piv_state = piv.get(self.COL_STATE, None)

                            if piv_state != self.STATE_DEPRECATED:

                                self.fdes[piv_idx][
                                    self.COL_STATE] = self.STATE_DEPRECATED
                                self.fdes[piv_idx][
                                    self.COL_DEPRECATED_DATE] = now_to_str()

                    if (new['Serial_Identifier'] == piv['Serial_Identifier'] and
                            new['Version'] == piv['Version']):

                        double = True

                if not double:

                    self.fdes.append(new)

    def deprecate_unused_fdes(self):

        if not self.published_fdes_ids:
            return

        for piv_idx, piv in enumerate(self.fdes.copy()):

            piv_state = piv.get(self.COL_STATE, None)

            if piv_state != self.STATE_DEPRECATED:

                if piv['ID_FDES'] not in self.published_fdes_ids:

                    self.fdes[piv_idx][self.COL_STATE] = self.STATE_DEPRECATED
                    self.fdes[piv_idx][self.COL_DEPRECATED_DATE] = now_to_str()

    def add_extra_fields(self, row):

        xtra = {
            self.COL_SOURCE: 'Inies',
            self.COL_IMPORT_DATE: now_to_str(),
            self.COL_STATE: self.STATE_VALID,
            self.COL_DEPRECATED_DATE: None
        }

        xtra.update(row)

        return xtra

    def merge_normes(self):

        for new in self.new_normes:

            replaced = False

            for piv_idx, piv in enumerate(self.normes.copy()):
                if piv['Norme_ID'] == new['Norme_ID']:
                    self.normes[piv_idx] = new
                    replaced = True
                    break

            if not replaced:
                self.normes.append(new)

        pass

    def merge_nomenclature(self):

        # we replace, since nomemclature is not versioned
        self.nomenclature = self.new_nomenclature

    def transfer_new_data(self, step: Step):

        self.new_fdes = step.get_fdes()
        self.new_normes = step.get_normes()
        self.new_nomenclature = step.get_nomenclature()
