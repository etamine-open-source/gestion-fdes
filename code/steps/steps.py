# -*- coding: utf-8 -*-
# Copyright (C) 2024 ETAMINE
# 
# gestion-fdes is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gestion-fdes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with etamine-fdes. If not, see <http://www.gnu.org/licenses/>.

import json
import operator
from concurrent.futures import as_completed, ThreadPoolExecutor
from datetime import datetime
from functools import reduce

from clients.clients import Inies as IniesClient
import ijson
from dotenv import load_dotenv
from xlsxwriter import Workbook
from abc import ABC
from helpers import clean_sheet_name
from progress.bar import Bar
import os
import shutil
import pandas as pd
from helpers import abs_path, IniesJSONDecoder, IniesJSONEncoder
from zeep.exceptions import Fault


class Step(ABC):

    def __init__(self):

        self.fdes = []
        self.nomenclature = []
        self.normes = []

        self.indics = []
        self.phases = []

    def get_fdes(self):
        return self.fdes

    def get_normes(self):
        return self.normes

    def get_nomenclature(self):
        return self.nomenclature

    def set_fdes(self, fdes):
        self.fdes = fdes

    def set_normes(self, normes):
        self.normes = normes

    def set_nomenclature(self, nomenclature):
        self.nomenclature = nomenclature

    def set_indics(self, indics):
        self.indics = indics

    def set_phases(self, phases):
        self.phases = phases

    def get_indics(self):
        return self.indics

    def get_phases(self):
        return self.phases


class FileStep(Step):

    def __init__(self):

        super().__init__()
        self.path = None
        self.load_path()

    def load_path(self):

        load_dotenv()
        self.path = os.getenv(self.PATH_ENV_KEY)


class ExcelLocal(FileStep):

    PATH_ENV_KEY = 'PATH_EXCEL_LOCAL'

    FILE = 'inies.xlsx'

    NORMES_WS = 'Normes'
    NORMES_INDICS_WS = 'Normes - Indicateurs'
    NORMES_PHASES_WS = 'Normes - Phases'
    NOMENCLATURE_WS = 'Nomenclature'

    ALL_NO_FDES_WS = [NORMES_WS, NOMENCLATURE_WS]

    def __init__(self):

        super().__init__()

        self.wb = None  # Workbook

    def build_file(self):

        self.init_file()

        self.add_fdes()
        self.add_normes()
        self.add_indics()
        self.add_phases()

        self.save_file()

    def save_file(self):
        print('Saving Excel file ...')
        self.wb.close()

    def add_indics(self):

        self.wb.add_worksheet(self.NORMES_INDICS_WS)
        ws = self.wb.get_worksheet_by_name(self.NORMES_INDICS_WS)

        row = 0
        header = self.indics[0].keys()
        ws.write_row(row=row, col=0, data=header)

        # then data
        for r_idx, norme in enumerate(self.indics):
            row = r_idx + 1
            for col, k in enumerate(header):
                val = norme.get(k, '')
                ws.write(row, col, val)

    def add_phases(self):

        self.wb.add_worksheet(self.NORMES_PHASES_WS)
        ws = self.wb.get_worksheet_by_name(self.NORMES_PHASES_WS)

        row = 0
        header = self.phases[0].keys()
        ws.write_row(row=row, col=0, data=header)

        # then data
        for r_idx, norme in enumerate(self.phases):
            row = r_idx + 1
            for col, k in enumerate(header):
                val = norme.get(k, '')
                ws.write(row, col, val)

    def add_normes(self):

        self.wb.add_worksheet(self.NORMES_WS)
        ws = self.wb.get_worksheet_by_name(self.NORMES_WS)

        # header first
        row = 0
        header = self.normes[0].keys()
        ws.write_row(row=row, col=0, data=header)

        # then data
        for r_idx, norme in enumerate(self.normes):
            row = r_idx + 1
            for col, k in enumerate(header):
                val = norme.get(k, '')
                ws.write(row, col, val)

    def split_fdes_by_norme(self):

        res = {}

        for fde in self.fdes:

            n_id = fde['Norme_ID']
            if n_id not in res:
                res[n_id] = []

            res[n_id].append(fde)

        return res

    def add_fdes(self):

        print('Adding FDEs to Excel file...')

        fdes = self.split_fdes_by_norme()

        # We create one sheet per nome
        for norme_id, all_fdes in fdes.items():

            norme_name = clean_sheet_name(all_fdes[0]['Norme_Name'])

            with Bar(norme_name, max=len(all_fdes)) as bar:

                self.wb.add_worksheet(norme_name)
                ws = self.wb.get_worksheet_by_name(norme_name)

                # header first
                row = 0
                header = all_fdes[0].keys()
                ws.write_row(row=row, col=0, data=header)

                # then data
                for r_idx, fde in enumerate(all_fdes):

                    row = r_idx + 1
                    bar.next()
                    for col, k in enumerate(header):
                        val = fde.get(k, '')
                        ws.write(row, col, val)

                bar.finish()

    @property
    def file_path(self):
        return f'{self.path}/{self.FILE}'

    def init_file(self):

        self.wb = Workbook(abs_path(self.file_path))


class RawUpdateLocal(FileStep):

    PATH_ENV_KEY = 'PATH_RAW_UPDATE'

    FDES_FILE = 'fdes.json'
    NOMENCLATURE_FILE = 'nomenclature.json'
    NORME_FILE = 'norme.json'

    def save(self):

        self.save_fdes()
        self.save_nomemclature()
        self.save_normes()

    def read(self):

        self.read_fdes()
        self.read_normes()
        self.read_nomenclature()

    @property
    def fdes_path(self):
        p = f'{self.path}/{self.FDES_FILE}'
        return abs_path(p)

    @property
    def normes_path(self):
        p = f'{self.path}/{self.NORME_FILE}'
        return abs_path(p)

    @property
    def nomenclature_path(self):
        p = f'{self.path}/{self.NOMENCLATURE_FILE}'
        return abs_path(p)

    def save_fdes(self):

        with (open(self.fdes_path, 'w')) as f:
            print('Saving FDEs to JSON')
            json.dump(self.fdes, f, cls=IniesJSONEncoder)

    def save_nomemclature(self):

        with (open(self.nomenclature_path, 'w')) as f:
            print('Saving Nomenclature to JSON')
            json.dump(self.nomenclature, f, default=str)

    def save_normes(self):

        with (open(self.normes_path, 'w')) as f:
            print('Saving Normes to JSON')
            json.dump(self.normes, f, default=str)

    def read_nomenclature(self):
        with (open(self.nomenclature_path, 'r')) as f:
            self.nomenclature = json.load(f)

    def read_normes(self):
        with (open(self.normes_path, 'r')) as f:
            self.normes = json.load(f)

    def read_fdes(self):
        with (open(self.fdes_path, 'r')) as f:
            self.fdes = json.load(f, cls=IniesJSONDecoder)

    @staticmethod
    def get_nips():

        nips = set()

        with (open(RawUpdateLocal.FDES_PATH, 'r')) as f:
            for i in ijson.items(f, 'item', use_float=True):
                if i is not None:

                    n = i['Indicateur_Set']['Norme_ID']

                    for ind in i['Indicateur_Set'][
                            'List_Indicateur_Quantities'][
                            'Indicateur_Quantity']:
                        nip = (n, ind['Indicateur_ID'], ind['Phase_ID'])
                        nips.add(nip)

            sorted(nips, key=lambda x: (x[0], x[1], x[2]))

        return nips


class RawPivotLocal(RawUpdateLocal):

    PATH_ENV_KEY = 'PATH_RAW_PIVOT'


class Inies(Step):

    def __init__(self):

        super().__init__()

        self.__client = IniesClient()
        self.__changed_ids = []

        self.__normes_ids = set()

        # Concurrency helpers
        self.__executor = ThreadPoolExecutor()
        self.__exec_tasks = []

    def read(self):

        self.fetch_all_fdes()
        self.fetch_normes()
        self.fetch_nomenclature()

    def read_since(self, since):

        self.fetch_fdes_since(since)
        self.fetch_normes()
        self.fetch_nomenclature()

    def fetch_all_fdes(self):

        dt = datetime(1990, 1, 1)
        self.fetch_fdes_since(dt)

    def fetch_fdes_since(self, since: datetime):

        self.__build_fdes_changed_ids(since)
        self.__build_fdes_data()

    def __build_fdes_changed_ids(self, since):

        print(f'Fetching IDs of changed rows since {since}')

        rows = self.__client.get_change_list(since)
        self.__changed_ids = [row['ID_FDES'] for row in rows]

        print(f'Found {len(self.__changed_ids)} changed FDES')

    def __build_fdes_data(self):

        for fdes_id in self.__changed_ids:
            # TODO Corriger cette erreur
            if fdes_id != 36345:
                f = self.__executor.submit(self.fetch_fdes, fdes_id)
                self.__exec_tasks.append(f)

        with Bar('Fetching FDES', max=len(self.__changed_ids)) as bar:
            for f in as_completed(self.__exec_tasks):
                bar.next()
                self.fdes.append(f.result())
            bar.finish()

    def __build_normes_ids(self):

        # Since Inies endpoints does not provide a list of all normes
        # we have to bruteforce it via a range of IDs
        self.__normes_ids.update(set(range(1, 10)))

        # We also add (if necessary) the normes that are referenced in the FDES
        for f in self.fdes:
            if f:
                self.__normes_ids.add(f['Indicateur_Set']['Norme_ID'])

    def fetch_normes(self):

        self.__build_normes_ids()

        # We have to silent server simili 404 errors
        # since we are bruteforcing the normes IDs
        for norme_id in self.__normes_ids:
            try:
                norme = self.__client.get_norme(norme_id)
                self.normes.append(norme)
            except Fault:
                pass

    def fetch_fdes(self, fdes_id):
        return self.__client.get_fdes_full_data(fdes_id)

    def fetch_nomenclature(self):
        self.nomenclature = self.__client.get_nomenclature()

    def get_all_fdes_ids(self):
        all_shorts = self.__client.get_all_short_data()
        return [f['ID_FDES'] for f in all_shorts]

    def get_pdf_name(self, fdes_id):
        return self.__client.get_fdes_file_name(fdes_id)

    def get_pdf_content(self, file_name):
        return self.__client.get_file_content(file_name)

    def save_pdf(self, content, file_name, path_dir):
        file_path = os.path.join(path_dir, file_name.replace('/', '_'))
        with open(file_path, 'wb') as f:
            f.write(content)
        return file_path


class Filter(Step):

    def filter_fdes(self, filters: dict):

        res = []

        for fdes in self.fdes:

            for key, val in filters.items():

                all_keys = key.split('>>')

                field_val = reduce(operator.getitem, all_keys, fdes)

                if field_val == val:
                    res.append(fdes)

        self.fdes = res


class Download(Step):

    FDES = []
    CACHE_DICT = {}
    LOT_DICT = {}
    ERROR_DICT = {}
    PATH_ARCHIVES = ""

    def set_archive_path(self, path):
        self.PATH_ARCHIVES = path

    def set_fdes_dict(self, csv_file):
        df = pd.read_excel(csv_file, sheet_name=0)
        self.FDES = df.astype(str)

    def fdes_in_cache(self, fdes_id):
        if fdes_id in self.CACHE_DICT.keys():
            return True
        else:
            return False

    def add_fdes_in_cache(self, fdes_id, pdf_path):
        self.CACHE_DICT[fdes_id] = pdf_path

    def lot_in_dict(self, lot):
        if lot in self.LOT_DICT.keys():
            return True
        else:
            return False

    def create_archive_dir(self, lot):
        lot_archive_path = os.path.join(self.PATH_ARCHIVES, lot)
        if not os.path.exists(lot_archive_path):
            os.makedirs(lot_archive_path, exist_ok=True)
            self.LOT_DICT[lot] = {
                "path": lot_archive_path,
                "df": pd.DataFrame(columns=self.FDES.columns)
            }
        return lot_archive_path

    def copy_pdf(self, fdes_id, lot_archive_path):
        file_name = self.CACHE_DICT[fdes_id].split('\\').pop()
        path = os.path.join(lot_archive_path, file_name)
        if not os.path.exists(path):
            shutil.copyfile(self.CACHE_DICT[fdes_id], path)
        return file_name

    def zip_all_dirs(self):
        for lot in self.LOT_DICT.keys():
            path_excel = os.path.join(self.LOT_DICT[lot]["path"],
                                      lot + "_recap.xlsx")
            path_archive = self.LOT_DICT[lot]["path"]
            self.LOT_DICT[lot]["df"].to_excel(excel_writer=path_excel)
            shutil.make_archive(path_archive, 'zip', path_archive)

    def set_pdf_name(self, id, file_name):
        r = "_".join(file_name.replace('/', '_').split('_')[3:])
        r = str(id) + "_" + r
        return r

    def add_error(self, id, sError):
        if id not in self.ERROR_DICT.keys():
            self.ERROR_DICT[id] = sError

    def add_line_in_lot_pdf(self, lot, row, pdf_name):
        add_row = [row[0], row[1], row[2], pdf_name]
        n = len(self.LOT_DICT[lot]["df"])
        self.LOT_DICT[lot]["df"].loc[n] = add_row

    def print_error_dict(self):
        path = os.path.join(self.PATH_ARCHIVES, "error.txt")
        with open(path, "w") as f:
            for key, value in self.ERROR_DICT.items():
                f.write('%s:%s\n' % (key, value))

    def print_fdes_dict(self):
        path = os.path.join(self.PATH_ARCHIVES, "general.xlsx")
        self.FDES.to_excel(excel_writer=path)
