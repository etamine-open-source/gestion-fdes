# -*- coding: utf-8 -*-
# Copyright (C) 2024 ETAMINE
# 
# gestion-fdes is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gestion-fdes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with etamine-fdes. If not, see <http://www.gnu.org/licenses/>.

import unittest

from steps.inies_merger import IniesMerger


class TestMerger(unittest.TestCase):

    def test_merge(self):

        new_data = [
            {
                'Serial_Identifier': 'dummy_id',
                'Version': '1.1',
            }
        ]

        old_data = [
            {
                'Serial_Identifier': 'dummy_id',
                'Version': '1.0',
                'Etat': 'Valide'
            },
            {
                'Serial_Identifier': 'another_id',
                'Version': '1.0',
                'Etat': 'Valide'
            }
        ]

        merger = IniesMerger()

        merger.fdes = old_data
        merger.new_fdes = new_data

        merger.merge()

        valid = [i for i in merger.fdes if i['Etat'] == 'Valide']
        deprecated = [i for i in merger.fdes if i['Etat'] == 'Obsolète']

        self.assertEqual(len(merger.fdes), 3)
        self.assertEqual(len(valid), 2)
        self.assertEqual(len(deprecated), 1)
