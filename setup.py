# -*- coding: utf-8 -*-
# Copyright (C) 2024 ETAMINE
# 
# gestion-fdes is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# gestion-fdes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with etamine-fdes. If not, see <http://www.gnu.org/licenses/>.



class env:
    '''
    Classe des variables d'environnement locales
    '''

    def __init__(self):
        self.FDES_URL = 'https://www.base-inies.fr/IniesV4/Services/IniesWebService.asmx?WSDL'
        self.FDES_LOGIN = ''
        self.FDES_PASSWORD = ''

        self.PATH_EXCEL_LOCAL = './../data/excel'
        self.PATH_RAW_UPDATE = './../data/raw_update'
        self.PATH_RAW_PIVOT = './../data/raw_pivot'
        self.PATH_LOGS = './../logs'

        self.LOG_LEVEL = 'INFO'

        self.DT_FORMAT = '%d/%m/%Y %H:%M'

        self.MAILER_SMTP = ''
        self.MAILER_PORT = ''
        self.MAILER_USER = ''
        self.MAILER_PASSWORD = ''
        self.MAILER_RECIPIENT = ''

    def get_env_input(self):
        '''
        Renseignement des variables d'environnement locales par l'user
        '''
        print('Entrez votre login INIES :')
        self.FDES_LOGIN = input()

        print('Entrez votre mot de passe INIES :')
        self.FDES_PASSWORD = input()

        print('Voulez-vous configurer une alerte par mail en cas d\'erreur de la mise à jour ? (o\\n)')
        ok = False

        while not ok:
            bMailer = input()
            if bMailer == 'o':
                ok = True

                print('Renseigner le serveur SMTP :')
                self.MAILER_SMTP = input()

                print('Renseigner le port SMTP :')
                self.MAILER_PORT = input()

                print('Renseigner le mail de l\'envoyeur :')
                self.MAILER_USER = input()

                print('Renseigner le mot de passe de l\'envoyeur :')
                self.MAILER_PASSWORD = input()

                print('Renseigner le mail du destinataire :')
                self.MAILER_RECIPIENT = input()

            elif bMailer != 'n':
                print('Veuillez renseigner une réponse valide :')

            else:
                ok = True

    def print_dotenv(self):
        """
        Impression de l'objet dans un fichier .env
        """
        ef = open('.env', 'w')
        for key, value in self.__dict__.items():
            ef.write(f"{key} = '{value}'\n")

        ef.close

        print('Fichier .env créé avec succès')

    def create_dotenv(self):
        """
        Création du fichier .env
        """
        self.get_env_input()
        self.print_dotenv()


ENV = env()
ENV.create_dotenv()
